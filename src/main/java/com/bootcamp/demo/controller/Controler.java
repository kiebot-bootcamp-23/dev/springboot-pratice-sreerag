package com.bootcamp.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api")
public class Controler {
    @RequestMapping("helo/{name}")
    public String GetName(@PathVariable("name") String name, Model model) {
        model.addAttribute("name", name);
        return "helo";

    }
}
